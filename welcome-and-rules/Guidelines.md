**Toast and Tech follows the guidelines set out by #CauseAScene:**

- Tech is not neutral
- Prioritize the most vulnerable
- Lack of inclusion is a risk management issue
- Intention without strategy is chaos

See the https://hashtagcauseascene.com/ for more information and to support this cause

*Diversity and Inclusion*

We seek always to include and lift up the least represented and most vulnerable people. Education is a way out of oppression. It is hard to learn tech when one if feeling oppressed and disrespected. We welcome and celebrate you no matter what and want to help you learn.

*Gender*

This space is gender-welcoming; whatever and however you express that is included and celebrated. Please let us know your pronouns so we can respect that from the start and make you feel welcome.

*Safety*

This space seeks to provide a safe space for all, but will always prioritize the most vulnerable and at risk. If you feel in danger in any way, please contact the admins. (Mainly tamouse.)

*Code of Conduct*

As a group, we also adhere to the principles outlined in https://www.contributor-covenant.org/version/2/0/code_of_conduct/

