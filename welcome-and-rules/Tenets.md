Toast and Tech is an informal study group that focuses on supporting  women, non-binary, and black, indigenous, and people of colour, as well as other marginalized and underrepresented people in technology, including people with disabilities, LGBTQ+ folks, and various intersectionalities. Toast and Tech first and foremost seeks to be inclusive and reduce harm caused by so much of the tech space.
The Tenets of Toast and Tech:

- make EVERYONE welcome, enthusiastically
- make ALL questions welcome, enthusiastically
- everyone is learning, everyone is teaching
- there is no single path, there are as many paths as people attending, as many paths as needed
- you don't need a project or tech work to attend, just come
- you don't need any experience to attend, just come
- support everyone who shows up
- make it safe to learn, safe to ask questions, safe to show up.

Possibly the MOST important tenant of all:

ToastAndTech can happen anywhere, be run by anyone; all it takes is letting folks know about it, and showing up to hold the space