**Channels:**

Channels can be created for specific uses at any time, but they can also be deleted for non-use at any time at the server admin's discretion.

*Current Channels:*

#general - this is the primary conversation channel. On-topic discussions can revolve around tech, tech careers, issues affecting women, non-binary folks, other LGBTIQ+ issues, Black, Indigenous, and POC issues, disability issues, and any other things pertenant to the well-being of people attending this server.

#session-planning - keep this channel clear to plan times to meet as a group in the voice channels.

#off-topic - random topics about anything at all. Please remember this is a *safe* space and consider others' feelings before posting.

#introductions - make your introductions here. Please be sure to let us know you pronouns so we can respect that.

*Voice Channels*

#Lounge - just hanging out and talking, instead of typing in the #general channel above

#Study Rooms 1 & 2 - schedule using of these rooms in the #session-planning room; if we find that we need more than two video / audio channels at the same time we can make more.
